#include "StdAfx.h"
#include "MemoryBlock.h"
#include "Logerof.h"

MemUnlockAuto::MemUnlockAuto(LPVOID sAddress)
	:sAddress(sAddress)
{
	if(!VirtualProtect(sAddress, sizeof(DWORD), PAGE_READWRITE, &oldFlag))
	{
		LOG_INT3(VF("Failed unlock memory at address: " << sAddress));
	}
}

MemUnlockAuto::~MemUnlockAuto()
{
	if(!VirtualProtect(sAddress, sizeof(DWORD), oldFlag, &oldFlag))
	{
		LOG_INT3(VF("Failed set  memory info back address: " << sAddress));
	}
}



MemoryBlock::MemoryBlock(Assembly const &Asm, bool const bRevesed)
	: sMemStart(Asm.blockAddress), sMemSize(Asm.blockSize)
{
	SetMemory(Asm.AsmCode, bRevesed); 
}

MemoryBlock::MemoryBlock(LPVOID sMemStart, size_t const sMemSize)
	: sMemStart(reinterpret_cast<LPVOID>(sMemStart)), sMemSize(sMemSize)
{
}

void MemoryBlock::SetMemory(char const *chMemory, bool bReversed) const {
	int iRevesedIter = this->sMemSize - 1;
	for (size_t i = 0; i < this->sMemSize; ++i)
    {
		char *ptr = ((char *)this->sMemStart + i); // Get Memory pointer
        MemUnlockAuto k_AutoLock(ptr); // unlocked memory and auto lock
		if (bReversed)
			*ptr = *(chMemory + ((this->sMemSize - 1) - i)); // put data to memory
		else
			*ptr = *(chMemory + i); // put data to memory
    }
}
#pragma once
#include <Windows.h>
#include <limits.h>

struct Assembly
{
    LPVOID blockAddress; // address
    BYTE blockSize; // block size
	char* AsmCode; // assembly code
	Assembly(){}
	Assembly(LPVOID Address, BYTE Size,char* Code)
		: blockAddress(Address), blockSize(Size), AsmCode(Code)
	{
	}
	~Assembly()
	{
		//if(AsmCode != NULL)
		{
			//delete AsmCode;
		}
	}
};

class MemUnlockAuto
{
	/*
		Memory auto locker
		Automate unlock/lock memory by constructer and destryctor
	*/
private:
	const LPVOID sAddress;
	DWORD oldFlag;
public:
	explicit MemUnlockAuto(LPVOID sAddress);
	
	 ~MemUnlockAuto();
};

class MemoryBlock
{
private:
	const LPVOID sMemStart;
	const size_t sMemSize;
public:
	explicit MemoryBlock(Assembly const &Asm, bool const bRevesed = false);

	explicit MemoryBlock(LPVOID sMemStart, size_t const sMemSize);

	template <typename T>
	explicit MemoryBlock(LPVOID sMemStart, T value, bool const bRevesed = false)
		: sMemStart(sMemStart), sMemSize(sizeof(value))
	{
		char* DataArray = new char[sMemSize];
		memcpy(DataArray, &value, sMemSize);
		SetMemory(DataArray, bRevesed);
		delete DataArray;
	}
    
	void SetMemory(char const *chMemory, bool bReversed = false) const;
};

#define REG_PATCH(addr, ...) {  char Bytes[] = {__VA_ARGS__};	MemoryBlock mem(Assembly((LPVOID)(addr + (int)GetModuleHandle(NULL)), sizeof(Bytes), Bytes)); }
#pragma once
#include "VString.h"
#ifdef _DEBUG // Class accepted only in debug mode
#include <string>

class Logerof
{
private:
	std::string FileName;
private: // Singlethon
	Logerof(void);
public:
	~Logerof(void);
	
	Logerof static GetInstance();

	void WriteLog(const char* Data); // Write log
};

#define LOG(t, x) Logerof::GetInstance().WriteLog((VString() << "[" << t << "]" << x).stdStr().c_str());
#define LOG_WARNING(x) LOG("WARNING", x)
#define LOG_ERROR(x)   LOG("ERROR", x)
#define LOG_INFO(x)    LOG("INFO", x)
#define LOG_DEBUG(x)   LOG("DEBUG", x)
#define LOG_INT3(x)    LOG("INT3", x) __asm int 3;

#define DEBUG_MSG(x) ::MessageBoxW(NULL, L#x, L"DEBUG", MB_OK);
#else

#define LOG(x) /**/
#define LOG_WARNING(x) /**/
#define LOG_ERROR(x) /**/
#define LOG_INFO(x) /**/
#define LOG_DEBUG(x) /**/
#define LOG_INT3(x) __asm int 3;
#define DEBUG_MSG(x) /**/
#endif
//
// Created by Гриша on 08.05.2019.
//
#include "stdafx.h"
#include "VString.h"

VString::VString() {

}

VString::operator std::string const() const throw() {
    return this->strBase;
}

std::string VString::stdStr() const {
    return this->strBase;
}

VString &VString::operator=(std::string rhs) {
    this->strBase = rhs;
    return *this;
}

VString &VString::operator=(const char *rhs) {
    this->strBase = rhs;
    return *this;
}

VString &VString::operator=(long long rhs) {
    char buffer[32] = {};
    sprintf_s(buffer, sizeof(buffer), "%lli", rhs);
    this->strBase = buffer;
    return *this;
}

VString &VString::operator=(void *rhs) {
    char buffer[32] = {};
    sprintf_s(buffer, sizeof(buffer), "0x%p", rhs);
    this->strBase = buffer;
    return *this;
}

VString &VString::operator=(double rhs) {
    char buffer[32] = {};
    sprintf_s(buffer, sizeof(buffer), "%lf", rhs);
    this->strBase = buffer;
    return *this;
}

//////////////
VString &VString::operator+(std::string rhs) {
    this->strBase += rhs;
    return *this;
}

VString &VString::operator+(const char *rhs) {
    this->strBase += rhs;
    return *this;
}

VString &VString::operator+(long long rhs) {
    char buffer[32] = {};
    sprintf_s(buffer, sizeof(buffer), "%lli", rhs);
    this->strBase += buffer;
    return *this;
}

VString &VString::operator+(void *rhs) {
    char buffer[32] = {};
    sprintf_s(buffer, sizeof(buffer), "0x%p", rhs);
    this->strBase += buffer;
    return *this;
}

VString &VString::operator+(double rhs) {
    char buffer[32] = {};
    sprintf_s(buffer, sizeof(buffer), "%g", rhs);
    this->strBase += buffer;
    return *this;
}

VString &VString::operator+(size_t rhs) {
    char buffer[32] = {};
    sprintf_s(buffer, sizeof(buffer), "%zu", rhs);
    this->strBase += buffer;
    return *this;
}

VString &VString::operator=(size_t rhs) {
    char buffer[32] = {};
    sprintf_s(buffer, sizeof(buffer), "%zu", rhs);
    this->strBase = buffer;
    return *this;
}

VString &VString::operator=(int rhs) {
    char buffer[32] = {};
    sprintf_s(buffer, sizeof(buffer), "%i", rhs);
    this->strBase = buffer;
	return *this;
}

VString &VString::operator+(int rhs) {
    char buffer[32] = {};
    sprintf_s(buffer, sizeof(buffer), "%i", rhs);
    this->strBase += buffer;
	return *this;
}



#include "StdAfx.h"
#include "Logerof.h"
#include <fstream>
#include <ctime>
#include <iostream>

#ifdef _DEBUG // Class accepted only in debug mode
Logerof::Logerof(void)
{
	//time_t now = time(0);
	//char dt[32] = {};
	//ctime_s(dt, sizeof(dt), &now);
	//FileName = dt;
	//FileName += "Dryads";
	//FileName += ".log";
	FileName = "Dryads.log";
}

Logerof::~Logerof(void)
{
}

Logerof Logerof::GetInstance()
{
	static Logerof Instance;
	return Instance;
}

void Logerof::WriteLog(const char* Data)
{
	std::ofstream ff(FileName.c_str(), std::ios::app | std::ios::out);
	if(ff.is_open() == false)
	{
		return;
	}
	// Log time
	time_t now = time(0);
	char *t = ctime(&now);
	if (t[strlen(t)-1] == '\n') t[strlen(t)-1] = '\0';
	//
	ff << "[" << t << "]" << Data << std::endl;
	ff.close();
	std::cout << "[" << t << "]" << Data << std::endl;
}
#endif
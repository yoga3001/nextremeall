#include "MemoryBlock.h"
#include "Logerof.h"

#ifdef _DEBUG
	#pragma comment(lib, "Dryads_Debug.lib")
#endif

#ifdef NDEBUG
	#pragma comment(lib, "Dryads_Release.lib")
#endif 

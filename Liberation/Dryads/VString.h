//
// Created by Гриша on 08.05.2019.
//

#ifndef VSTRING_VSTRING_H
#define VSTRING_VSTRING_H

#include <string>

class VString {
private:
    std::string strBase;
public: // operators
    operator std::string const() const throw();

    VString();

    template<typename T>
    VString(T const &rhs) {
        operator=(rhs);
    }

public:
    std::string stdStr() const;

public:// Operator
    VString &operator=(std::string rhs);

    VString &operator=(const char *rhs);

    VString &operator=(long long rhs);

    VString &operator=(int rhs);

    VString &operator=(void *rhs);

    VString &operator=(double rhs);

    VString &operator=(size_t rhs);

    VString &operator+(std::string rhs);

    VString &operator+(const char *rhs);

    VString &operator+(long long rhs);

    VString &operator+(void *rhs);

    VString &operator+(double rhs);

    VString &operator+(size_t rhs);

    VString &operator+(int rhs);

    template<typename T>
    VString &operator+=(T const &rhs) {
        this->strBase += VString(rhs).strBase;
        return *this;
    }

    template<typename T>
    VString &operator<<(T const &rhs) {
        this->strBase += VString(rhs).strBase;
        return *this;
    }
};

#define VF(x) (VString() << x).stdStr().c_str()
#endif //VSTRING_VSTRING_H

#pragma once

#include <windows.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#pragma comment (lib, "Ws2_32.lib")

#ifdef _DEBUG
#pragma comment(lib, "Vishnu_Debug.lib")
#endif

#ifdef NDEBUG
#pragma comment(lib, "Vishnu_Release.lib")
#endif 
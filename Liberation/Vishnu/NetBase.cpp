#include "stdafx.h"
#include "NetBase.h"
#include <future>

namespace Vishnu {
	NetBase::NetBase()
	{
	}

	NetBase::~NetBase()
	{
	}
	
	void NetBase::BindAddr(const char * _strAddr, unsigned short _stServerPort)
	{
		this->strAddr = _strAddr;
		this->stServerPort = _stServerPort;
	}

	void NetBase::BinHandler(RECEV_HANDLER FuncRecev)
	{
		this->OnRecev = FuncRecev;
	}

	void NetBase::Send(SOCKET Client, char * strData, size_t stDataSize)
	{
		std::mutex kLock;
		kLock.lock();
		if (Client == INVALID_SOCKET)
		{
			return;
		}

		if (stDataSize > PACKET_BODY_SIZE)
		{
			LOG_INFO("Packet is too long for send: " << stDataSize);
			return;
		}

		char* kPacket = nullptr;
		const int iPacketSize = PACKET_HEAD_LEN + stDataSize;
		kPacket = new char[iPacketSize];
		memcpy(kPacket, &stDataSize, PACKET_HEAD_LEN);
		memcpy(kPacket + PACKET_HEAD_LEN, &strData, stDataSize);
		if (send(Client, kPacket, iPacketSize, 0) == SOCKET_ERROR)
		{
			LOG_INFO("Failed send packet: " << WSAGetLastError());
		}
		if (kPacket) { delete[] kPacket; }
		kLock.unlock();
	}

	void NetBase::OnConnect(SOCKET Client)
	{
		//std::mutex kLock;
		//kLock.lock();
		/*
		auto kSockAddr = (sockaddr_in *)Client;
		if (kSockAddr)
		{
		LOG_INFO("Client has connected: " << kSockAddr->sin_addr << kSockAddr->sin_port);
		}
		*/
		const int iBufferLen = MAX_PACKET_LEN;
		char chBuffer[MAX_PACKET_LEN] = {};
		int iResult = 0;
		//do {
		//sockaddr_in sockFrom;
		//int iSockFromSize = sizeof(sockFrom);
		iResult = recv(Client, chBuffer, iBufferLen, 0/*, (SOCKADDR *)& sockFrom, &iSockFromSize*/);
		//} while (iResult > 0 || iResult < MAX_PACKET_LEN);
		//TODO: Make protect for server head

		if (iResult < 0 || iResult > MAX_PACKET_LEN)
		{
			LOG_INFO("Packet size is too big or too less " << iResult);
			goto _EXIT;
		}

		PACKET_HEAD_TYPE stRealPacketLen = 0;
		memcpy(&stRealPacketLen, chBuffer, PACKET_HEAD_LEN);
		if (stRealPacketLen > PACKET_BODY_SIZE)
		{
			LOG_INFO("Packet body size is too big" << stRealPacketLen);
			goto _EXIT;
		}

		if (stRealPacketLen > iResult - PACKET_HEAD_LEN || stRealPacketLen < 0)
		{
			LOG_INFO("Broken packet size" << stRealPacketLen);
			goto _EXIT;
		}

		if (this->OnRecev == nullptr)
		{
			LOG_INFO("OnRecev is NULL");
			goto _EXIT;
		}

		this->OnRecev(stRealPacketLen, chBuffer + PACKET_HEAD_LEN, Client, this);
	_EXIT:
		LOG_INFO("DUMMY");
		closesocket(Client);
		//kLock.unlock();
	}
}
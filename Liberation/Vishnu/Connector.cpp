#include "stdafx.h"
#include "Connector.h"
#include <iostream>
#include <future>

namespace Vishnu {
	Connector::Connector()
	{
	}

	Connector::~Connector()
	{
	}

	bool Connector::Connect()
	{
		int iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
		if (iResult != NOERROR)
		{
			std::cerr << "WSAStartup failed! Return code: " << iResult;
			return false;
		}

		ZeroMemory(&Hints, sizeof(Hints));
		this->Hints.ai_family = AF_UNSPEC;
		this->Hints.ai_socktype = SOCK_STREAM;
		this->Hints.ai_protocol = IPPROTO_TCP;
		//this->Hints.ai_flags = AI_PASSIVE;

		struct addrinfo *HintResult = NULL;
		iResult = getaddrinfo(this->strAddr.c_str(), std::to_string(this->stServerPort).c_str(), &this->Hints, &HintResult);
		if (iResult != NOERROR)
		{
			std::cerr << "getaddrinfo failed! Return code: " << iResult;
			goto __ERROR;
		}

		for (struct addrinfo* ptr = HintResult; ptr != nullptr; ptr = ptr->ai_next)
		{
			ConnectSocket = socket(ptr->ai_family, ptr->ai_socktype, ptr->ai_protocol);
			if (ConnectSocket == INVALID_SOCKET)
			{
				std::cerr << "Unable to connect to server!";
				goto __ERROR;
			}

			iResult = connect(ConnectSocket, ptr->ai_addr, (int)ptr->ai_addrlen);
			if (iResult == SOCKET_ERROR) {
				closesocket(ConnectSocket);
				ConnectSocket = INVALID_SOCKET;
				continue;
			}
			break;
		}
		freeaddrinfo(HintResult);

		if (ConnectSocket == INVALID_SOCKET) 
		{
			std::cerr << "Unable to connect to server!";
			goto __ERROR;
		}
		char* connectInfo = "Success";
		this->Send(ConnectSocket, connectInfo, strlen(connectInfo));
		this->OnConnect(ConnectSocket);
		//std::thread(&Connector::OnConnectSuccess, this).detach();
		return true;
	__ERROR:
		WSACleanup();
		if (HintResult) freeaddrinfo(HintResult);
		if (ConnectSocket != INVALID_SOCKET)	closesocket(ConnectSocket);
		return false;
	}
}
#pragma once
#include <string>
#include "NetBase.h"

namespace Vishnu {
	class Acceptor: public NetBase
	{
	public:
		typedef void(*RECEV_HANDLER)(size_t dataLen, const char* RecevData, SOCKET Client, NetBase* instance);
	public:
		Acceptor();
		~Acceptor();
		
		bool Init();

		void ProcessStart();
	};
}

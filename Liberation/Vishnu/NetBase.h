#pragma once
#include <WinSock2.h>
#include <ws2tcpip.h>
namespace Vishnu {
	typedef size_t PACKET_HEAD_TYPE;
	constexpr size_t PACKET_HEAD_LEN = sizeof(PACKET_HEAD_TYPE);
	constexpr size_t PACKET_BODY_SIZE = 128;
	constexpr size_t MAX_PACKET_LEN = PACKET_HEAD_LEN + PACKET_BODY_SIZE;

	class NetBase
	{
	public:
		typedef void(*RECEV_HANDLER)(size_t dataLen, const char* RecevData, SOCKET Client, NetBase* instance);
	public:
		NetBase();
		~NetBase();

		void Send(SOCKET Client, char* strData, size_t stDataSize);

		void BindAddr(const char* _strAddr, unsigned short _stServerPort);

		void BinHandler(RECEV_HANDLER FuncRecev);
	protected:
		void OnConnect(SOCKET Client);
	protected:
		/*Server IP*/
		std::string strAddr;
		/*Server port*/
		unsigned short stServerPort;
		/*On success recev function*/
		RECEV_HANDLER OnRecev;		
		/*Winsock data*/
		WSADATA wsaData;
		/**/
		struct addrinfo Hints;
		SOCKET ListenSocket = INVALID_SOCKET;
	};
}
#include "stdafx.h"
#include "Acceptor.h"
#include <iostream>
#include <future>

namespace Vishnu
{
	Acceptor::Acceptor()
	{
	}

	Acceptor::~Acceptor()
	{
	}

	bool Acceptor::Init()
	{
		int iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
		if (iResult != NOERROR)
		{
			std::cerr << "WSAStartup failed! Return code: " << iResult;
			return false;
		}

		ZeroMemory(&Hints, sizeof(Hints));
		this->Hints.ai_family = AF_INET;
		this->Hints.ai_socktype = SOCK_STREAM;
		this->Hints.ai_protocol = IPPROTO_TCP;
		this->Hints.ai_flags = AI_PASSIVE;

		struct addrinfo *HintResult = NULL;
		iResult = getaddrinfo(this->strAddr.c_str(), std::to_string(this->stServerPort).c_str(), &this->Hints, &HintResult);
		if (iResult != NOERROR)
		{
			std::cerr << "getaddrinfo failed! Return code: " << iResult;
			goto __ERROR;
		}

		ListenSocket = socket(HintResult->ai_family, HintResult->ai_socktype, HintResult->ai_protocol);
		if (ListenSocket == INVALID_SOCKET)
		{
			std::cerr << "Socket failed! Return code: " << WSAGetLastError();
			goto __ERROR;
		}

		iResult = bind(ListenSocket, HintResult->ai_addr, (int)HintResult->ai_addrlen);
		if (iResult == SOCKET_ERROR) {
			std::cerr << "bind failed! Return code: " << WSAGetLastError();
			goto __ERROR;
		}

		freeaddrinfo(HintResult);
		iResult = listen(ListenSocket, SOMAXCONN);
		if (iResult == SOCKET_ERROR)
		{
			std::cerr << "listen failed! Return code: " << WSAGetLastError();
			goto __ERROR;
		}

		LOG_INFO(VString() << "Server start at addr: " << this->strAddr << std::to_string(this->stServerPort));
		return true;
	__ERROR:
		WSACleanup();
		if (HintResult) freeaddrinfo(HintResult);
		if (ListenSocket != INVALID_SOCKET)	closesocket(ListenSocket);
		return false;
	}

	void Acceptor::ProcessStart()
	{
		SOCKET ClientSocket = INVALID_SOCKET;
		while (true)
		{
			ClientSocket = accept(ListenSocket, NULL, NULL);
			if (ClientSocket == INVALID_SOCKET) {
				std::cerr << "accept failed! Return code: " << WSAGetLastError();
				goto _EXIT;
			}
			//std::async(std::launch::async, &Acceptor::OnConnect, this, ClientSocket);
			std::thread(&Acceptor::OnConnect, this, ClientSocket).join();
		}

		const int iResult = shutdown(ClientSocket, SD_SEND);
		if (iResult == SOCKET_ERROR) {
			std::cout << "shutdown failed with error:" << WSAGetLastError();
			goto _EXIT;
		}

		_EXIT:
		if(ClientSocket != INVALID_SOCKET) closesocket(ClientSocket);
		WSACleanup();
	}
}

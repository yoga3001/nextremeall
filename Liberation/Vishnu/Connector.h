#pragma once
#include "NetBase.h"
namespace Vishnu {
	class Connector : public NetBase
	{
	public:
		Connector();
		
		~Connector();
		
		bool Connect();
	private:
		SOCKET ConnectSocket = INVALID_SOCKET;
	};
}
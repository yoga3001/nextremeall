// nExtremeServer.cpp: ���������� ����� ����� ��� ����������� ����������.
//

#include "stdafx.h"
#include "Dryads.h"
#include "Vishnu.h"
#include "Acceptor.h"
#include "Connector.h"

void OnRecev(size_t dataLen, const char* RecevData, SOCKET Client, Vishnu::NetBase* instance)
{
	
	char Data[Vishnu::PACKET_BODY_SIZE] = {};
	{
		char Bytes[] = { 0x03, 0x44, 0xE4 }; // ADD EAX,DWORD PTR SS:[ESP+38]
		int ENCRYPTO_ADDR_1 = 0x01B73FC6;
		int ENCRYPTO_ADDR_2 = 0x01B73EE6;
		memcpy(&Data, Bytes, sizeof(Bytes));
		memcpy(&Data, &ENCRYPTO_ADDR_1, sizeof(ENCRYPTO_ADDR_1));
		memcpy(&Data, &ENCRYPTO_ADDR_2, sizeof(ENCRYPTO_ADDR_2));
	}
	
	instance->Send(Client, Data, sizeof(Data));
}

void OnRecevTest(size_t dataLen, const char* RecevData, SOCKET Client, Vishnu::NetBase* instance)
{
	char Data[] = "Hello world";
	instance->Send(Client, Data, sizeof(Data));
}

int main(int argc, char* argv[])
{
#define CON_INFO "127.0.0.1", 1899
	if (argc == 2)
	{
		auto kServer = Vishnu::Connector();
		kServer.BindAddr(CON_INFO);
		kServer.BinHandler(OnRecevTest);
		if (kServer.Connect())
		{
			LOG_INT3("Failed connected to server");
		}
	}
	else
	{
		auto kServer = Vishnu::Acceptor();
		kServer.BindAddr(CON_INFO);
		kServer.BinHandler(OnRecev);
		if (kServer.Init())
		{
			kServer.ProcessStart();
		}
	}
    return 0;
}


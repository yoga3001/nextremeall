// TestDLLInject.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>

volatile int GetRandom(int iValue)
{
	if (iValue > 10)
	{
		return rand() % 10 + iValue;
	}
	else
	{
		return rand() % 100 + iValue;
	}
}

volatile int getValue(int iValue)
{
	return GetRandom(iValue);
}

int _tmain(int argc, _TCHAR* argv[])
{
	while(true)
	{
		int iVal = 0;
		std::cout << "Enter value: ";
		std::cin >> iVal;
		std::cout << getValue(iVal) << std::endl;
	}
	return 0;
}


// dllmain.cpp: ���������� ����� ����� ��� ���������� DLL.
#include "stdafx.h"
#include "Dryads.h"

DllExport int Hock()
{
	return 1;
}

void Init()
{
	{// Encrypto
		// ADD EAX,EBP
		REG_PATCH(0x000C58F0, 0x01, 0xE8);
		REG_PATCH(0x000C5830, 0x01, 0xE8);
	}
}

BOOL APIENTRY DllMain( HMODULE hModule,
                       DWORD  ul_reason_for_call,
                       LPVOID lpReserved
					 )
{
	switch (ul_reason_for_call)
	{
	case DLL_PROCESS_ATTACH:
		Init();
		break;
	case DLL_THREAD_ATTACH:
	case DLL_THREAD_DETACH:
	case DLL_PROCESS_DETACH:
		break;
	}
	return TRUE;
}


// nExtreme.cpp : Defines the entry point for the DLL application.
//

#include "stdafx.h"
#include "Dryads.h"
#include <time.h>
#ifdef _MANAGED
#pragma managed(push, off)
#endif

DllExport int Hock()
{
	return 1;
}

void InitDragonicaFunc()
{
	// encrypto
	{
		#define ENCRYPT_BYTES 0x03, 0x44, 0xE4 // ADD EAX,DWORD PTR SS:[ESP+38]
#ifdef _DEBUG
		REG_PATCH(0x00B53FC3, ENCRYPT_BYTES);
		REG_PATCH(0x00B53EE6, ENCRYPT_BYTES);
#else	
		REG_PATCH(0x00B53FC3, ENCRYPT_BYTES);
		REG_PATCH(0x00B53EE6, ENCRYPT_BYTES);
#endif // _DEBUG
	}
	//Server version
	{
		#define SERVER_VERSION_BYTES 0x31, 0x00, 0x30, 0x00, 0x2E, 0x00, 0x30, 0x00, 0x32, 0x00, 0x2E, 0x00, 0x32, 0x00, 0x39, 0x00, 0x2E, 0x00, 0x39
#ifdef _DEBUG
		//REG_PATCH(0x012EC0C0, SERVER_VERSION_BYTES);
#else
		//REG_PATCH(0x021FC0C0, SERVER_VERSION_BYTES);
		//REG_PATCH(0x020BC0C0, SERVER_VERSION_BYTES);
#endif
	}
#ifdef _DEBUG
	{//Dissable duplicate client error
		char Bytes[] = {0xEB}; //(LPVOID)0x00FEF4FC, 1, Bytes
		//MemoryBlock mem(Assembly((LPVOID)0x0127F4FC, 1, Bytes));
	}
	{//Max level for open market and shop
		//REG_PATCH(0x001B4597, 0x1); // Set min lv = 1
		//REG_PATCH(0x001B45A0, 0x1); // Set min class no = 1
		//REG_PATCH(0x001B45A9, 0x1); // Set min Drakan hydra min class = 1
		//REG_PATCH(0x001B45B2, 0x1); // Set min Drakan warrior min class no = 1
	}
#endif
}

BOOL APIENTRY DllMain( HMODULE hModule,
                       DWORD  ul_reason_for_call,
                       LPVOID lpReserved
					 )
{
    switch (ul_reason_for_call)
    {
        case DLL_PROCESS_ATTACH:
			{
				//DEBUG_MSG("DLL_PROCESS_ATTACH");
				//LOG_INFO("DLL_PROCESS_ATTACH");
				DEBUG_MSG(VString() << "Hock func in address: " << &Hock);
				LOG_INFO(VString() << "Hock func in address: " << &Hock);
				LOG_INFO(VString() << "GetModuleHandle(NULL): " << GetModuleHandle(NULL));
				InitDragonicaFunc();
			}break;
        case DLL_THREAD_ATTACH:
			{
				//DEBUG_MSG("DLL_THREAD_ATTACH");
				//LOG_INFO("DLL_THREAD_ATTACH");
			}break;
        case DLL_THREAD_DETACH:
			{
				//DEBUG_MSG("DLL_THREAD_DETACH");
				//LOG_INFO("DLL_THREAD_DETACH");
			}break;
        case DLL_PROCESS_DETACH:
			{
				//DEBUG_MSG("DLL_PROCESS_DETACH");
				//LOG_INFO("DLL_PROCESS_DETACH");
			}break;
    }

	return TRUE;
}

#ifdef _MANAGED
#pragma managed(pop)
#endif

